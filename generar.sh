#!/bin/bash

if [ ! -d resultados ];
then
	mkdir resultados
fi


for file in "$@"
do
	for i in {3,5}
	do
		instance="$(basename "$file")"
		instance="${instance%.*}"
		resultsfile="resultados/$instance"_"$i"_results.txt
		./Debug/P3Metaheuristics $file $i > "$resultsfile"

		output="resultados/$instance"_"$i"
		instanceName="$(sed 's/_/ /g' <<< $instance)"


outputSA=$output"_SA.png"
outputTS=$output"_TS.png"
outputGRASP=$output"_GRASP.png"
outputIG=$output"_IG.png"

cat << _end_ | gnuplot
	set terminal png
	set output "$outputSA"
	set key right bottom
	set title "$instanceName con $i mochilas"
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	plot '$resultsfile' using 1 with linespoints pi 10000 title 'Current SA', '$resultsfile' using 2 with linespoints pi 10000 title 'Best SA'
_end_
	
	echo -e "Generado: $output"

cat << _end_ | gnuplot
	set terminal png
	set output "$outputTS"
	set key right bottom
	set title "$instanceName con $i mochilas"
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	plot '$resultsfile' using 3 with linespoints pi 10000 title 'Current TS', '$resultsfile' using 4 with linespoints pi 10000 title 'Best TS'
_end_

	echo -e "Generado: $output"

cat << _end_ | gnuplot
	set terminal png
	set output "$outputGRASP"
	set key right bottom
	set title "$instanceName con $i mochilas"
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	plot '$resultsfile' using 5 with linespoints pi 10000 title 'Current GRASP', '$resultsfile' using 6 with linespoints pi 10000 title 'Best GRASP'
_end_

	echo -e "Generado: $output"

cat << _end_ | gnuplot
	set terminal png
	set output "$outputIG"
	set key right bottom
	set title "$instanceName con $i mochilas"
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	plot '$resultsfile' using 7 with linespoints pi 10000 title 'Current IG', '$resultsfile' using 8 with linespoints pi 10000 title 'Best IG'
_end_

	echo -e "Generado: $output"


	done


done



#echo -e "Graphic generated: $2"