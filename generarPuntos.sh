#!/bin/bash

if [ ! -d resultados ];
then
	mkdir resultados
fi


for file in "$@"
do
	for i in {3,5}
	do
		instance="$(basename "$file")"
		instance="${instance%.*}"
		resultsfile="resultados/$instance"_"$i"_results.txt
		./Debug/P3Metaheuristics $file $i > "$resultsfile"

		output="resultados/$instance"_"$i".png
		instanceName="$(sed 's/_/ /g' <<< $instance)"

cat << _end_ | gnuplot
	set terminal png
	set output "$output"
	set key right bottom
	set title "$instanceName con $i mochilas"
	set xlabel "Numero de ejecuciones"
	set ylabel "Fitness"
	plot '$resultsfile' using 1 with points pt 7 ps 0.6 title 'Current SA', '$resultsfile' using 2 with linespoints pi 10000 title 'Best SA', '$resultsfile' using 3 with points pt 7 ps 0.6 title 'Current TS', '$resultsfile' using 4 with linespoints pi 10000 title 'Best TS', '$resultsfile' using 5 with points pt 7 ps 0.6 title 'Current GRASP', '$resultsfile' using 6 with linespoints pi 10000 title 'Best GRASP', '$resultsfile' using 7 with points pt 7 ps 0.6 title 'Current IG', '$resultsfile' using 8 with linespoints pi 10000 title 'Best IG'
_end_

	echo -e "Generado: $output"

	done


done



#echo -e "Graphic generated: $2"